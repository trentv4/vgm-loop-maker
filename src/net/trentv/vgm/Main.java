package net.trentv.vgm;

import java.awt.Color;
import java.util.ArrayList;

import net.trentv.vgm.components.Component;
import net.trentv.vgm.components.ComponentInstrumentAdjuster;
import net.trentv.vgm.components.ComponentLoopButton;
import net.trentv.vgm.components.ComponentMusicTile;
import net.trentv.vgm.components.ComponentPauseButton;
import net.trentv.vgm.components.ComponentTempoAdjuster;

public class Main
{
	public static Player player;
	public static Renderer r = new Renderer();
	public static ArrayList<Component> l;

	public static void main(String[] args)
	{
		l = new ArrayList<Component>();
		l.add(new ComponentPauseButton("play.png", 10,29,60,60));
		l.add(new ComponentTempoAdjuster(150,29,20,20, true));
		l.add(new ComponentTempoAdjuster(150,69,20,20, false));
		l.add(new ComponentInstrumentAdjuster(220,29,20,20, true));
		l.add(new ComponentInstrumentAdjuster(220,69,20,20, false));
		l.add(new ComponentLoopButton("loop.png", 80,29,60,60));
		for(int g = 0; g < 16; g++)
		{
			for(int i = 0; i < 16; i++)
			{
				l.add(new ComponentMusicTile(null, 10 + 30*g, 99 + 30*i, 25, 25, g, i));
			}
		}
		player = new Player();
		player.isPlaying = false;
		player.volume = 80;
		player.speed = 300;

		player.musicData = new boolean[16][16];
		player.instrData = new int[16][16];
		
		Thread t = new Thread(new Display());
		t.start();
		player.init();
	}
	
	public static void draw()
	{
		r.drawImage("bar.png", 5 + 30*player.trackPosition, 94, 35, 485);
		r.drawImage("delay.png", 150, 55, 33, 11);
		r.drawImage("instrument.png", 220, 55, 66, 8);
		r.drawImage("name.png", 6, 586, 222, 11);
		r.drawImage("title.png", 6, 6, 158, 16);
		r.drawText(""+player.speed, Color.WHITE, 186, 55, 2);
		r.drawText(""+player.instrument, Color.WHITE, 290, 55, 2);
		for(int i = 0; i < l.size(); i++)
		{
			l.get(i).eval(InputMapper.getMousePos(),r);
			l.get(i).draw(r);
		}
	}
}
