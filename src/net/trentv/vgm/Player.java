package net.trentv.vgm;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Synthesizer;

public class Player
{
	public volatile int[][] instrData;
	public int instrument = 0;
	public volatile boolean[][] musicData;
	public volatile int trackPosition = 0;
	public volatile int speed = 250;
	public volatile boolean isLooping = true;
	public volatile boolean isPlaying = true;
	public volatile int volume;
	
	public void init()
	{
		try
		{
			Synthesizer synth = MidiSystem.getSynthesizer();
			synth.open();
			Instrument[] inst = synth.getLoadedInstruments();
			MidiChannel[] c = synth.getChannels();
			for(int i = 0; i < c.length; i++)
			{
				c[i].programChange(inst[i].getPatch().getProgram());
			}
			while(true)
			{
				if(isPlaying)
				{
					trackPosition++;
					if(trackPosition >= musicData[1].length)
					{
						trackPosition = 0;
						if(!isLooping) 
						{
							isPlaying = false;
						}
					}
					if(isPlaying)
					{
						for(int i = 0; i < musicData[trackPosition].length; i++)
						{
							if(musicData[trackPosition][i])
							{
								c[instrData[trackPosition][i]].noteOn(60 + i, volume);
							}
						}
						Thread.sleep(speed);
					}
				}
			}
		} catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}