package net.trentv.vgm.components;

import java.awt.Color;

import net.trentv.vgm.InputMapper;
import net.trentv.vgm.Renderer;

public class Component
{
	public int x;
	public int y;
	public int xSize;
	public int ySize;

	public boolean renderThisTick = true;
	
	public Component(int x, int y, int xS, int yS)
	{
		this.x = x;
		this.y = y;
		this.xSize = xS;
		this.ySize = yS;
	}
	
	public void draw(Renderer r)
	{
		if(renderThisTick)
		{
			r.drawRectangle(Color.RED, x, y, xSize, ySize); return;
		}
		else
		{
			renderThisTick = true;
		}
	}
	
	public void onClick(Renderer r)
	{
		
	}
	
	public void onHover(Renderer r)
	{
		r.drawRectangle(Color.GRAY, x, y, xSize, ySize);
		renderThisTick = false;
	}
	
	public void eval(int[] mouse, Renderer r)
	{
		if(mouse[0] > x)
		{
			if(mouse[0] < x + xSize)
			{
				if(mouse[1] > y)
				{
					if(mouse[1] < y + ySize)
					{
						if(InputMapper.getMouseButtons()[0])
						{
							InputMapper.setMouseUp(0);
							onClick(r);
						}
						onHover(r);
					}
				}
			}
		}
	}
}
