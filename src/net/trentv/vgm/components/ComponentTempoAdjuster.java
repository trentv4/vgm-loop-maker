package net.trentv.vgm.components;

import net.trentv.vgm.Main;
import net.trentv.vgm.Renderer;

public class ComponentTempoAdjuster extends Component
{
	private String texture = "";
	private boolean isUp;
	
	public ComponentTempoAdjuster(int x, int y, int xS, int yS, boolean isUp)
	{
		super(x, y, xS, yS);
		this.isUp = isUp;
		if(isUp) this.texture = "up.png";
		else this.texture = "down.png";
	}
	
	@Override
	public void draw(Renderer r)
	{
		r.drawImage(texture, x, y, xSize, ySize);
	}

	@Override
	public void onClick(Renderer r)
	{
		if(isUp) Main.player.speed += 10;
		else Main.player.speed -= 10;
	}
}
