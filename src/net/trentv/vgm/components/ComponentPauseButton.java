package net.trentv.vgm.components;

import net.trentv.vgm.Main;
import net.trentv.vgm.Renderer;

public class ComponentPauseButton extends Component
{
	private String texture = "";
	
	public ComponentPauseButton(String texture, int x, int y, int xS, int yS)
	{
		super(x, y, xS, yS);
		this.texture = texture;
	}
	
	@Override
	public void draw(Renderer r)
	{
		if(!Main.player.isPlaying) this.texture = "play.png";
		else this.texture = "pause.png";
		r.drawImage(texture, x, y, xSize, ySize);
	}

	@Override
	public void onClick(Renderer r)
	{
		if(Main.player.isPlaying)
		{
			Main.player.isPlaying = false;
		}
		else
		{
			if(Main.player.trackPosition >= Main.player.musicData[1].length)
			{
				Main.player.trackPosition = 0;
			}
			Main.player.isPlaying = true;
		}
	}
}
