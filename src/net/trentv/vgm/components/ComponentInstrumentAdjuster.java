package net.trentv.vgm.components;

import net.trentv.vgm.Main;
import net.trentv.vgm.Renderer;

public class ComponentInstrumentAdjuster extends Component
{
	private String texture = "";
	private boolean isUp;
	
	public ComponentInstrumentAdjuster(int x, int y, int xS, int yS, boolean isUp)
	{
		super(x, y, xS, yS);
		this.isUp = isUp;
		if(isUp) this.texture = "up.png";
		else this.texture = "down.png";
	}
	
	@Override
	public void draw(Renderer r)
	{
		r.drawImage(texture, x, y, xSize, ySize);
	}

	@Override
	public void onClick(Renderer r)
	{
		if(isUp)
		{
			if(Main.player.instrument < 15)
			{
				Main.player.instrument++;
				fill(Main.player.instrument);
			}
		}
		else
		{
			if(Main.player.instrument > 0)
			{
				Main.player.instrument--;
				fill(Main.player.instrument);
			}
		}
	}
	
	private void fill(int instrument)
	{
		for(int i = 0; i < Main.player.instrData.length; i++)
		{
			for(int g = 0; g < Main.player.instrData[i].length; g++)
			{
				Main.player.instrData[i][g] = instrument;
			}
		}
	}
}
