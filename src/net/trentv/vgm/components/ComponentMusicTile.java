package net.trentv.vgm.components;

import net.trentv.vgm.Main;
import net.trentv.vgm.Renderer;

public class ComponentMusicTile extends Component
{
	private String texture = "";
	private int track;
	private int pitch;
	
	public ComponentMusicTile(String texture, int x, int y, int xS, int yS, int track, int pitch)
	{
		super(x, y, xS, yS);
		this.texture = texture;
		this.track = track;
		this.pitch = pitch;
	}
	
	@Override
	public void draw(Renderer r)
	{
		if(!Main.player.musicData[track][pitch]) this.texture = "tiledisable.png";
		else this.texture = "tileenable.png";
		r.drawImage(texture, x, y, xSize, ySize);
	}

	@Override
	public void onClick(Renderer r)
	{
		Main.player.musicData[track][pitch] = !Main.player.musicData[track][pitch];
	}
}
