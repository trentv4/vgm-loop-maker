package net.trentv.vgm.components;

import net.trentv.vgm.Main;
import net.trentv.vgm.Renderer;

public class ComponentLoopButton extends Component
{
	private String texture = "";
	
	public ComponentLoopButton(String texture, int x, int y, int xS, int yS)
	{
		super(x, y, xS, yS);
		this.texture = texture;
	}
	
	@Override
	public void draw(Renderer r)
	{
		if(Main.player.isLooping) this.texture = "loop.png";
		else this.texture = "noloop.png";
		r.drawImage(texture, x, y, xSize, ySize);
	}

	@Override
	public void onClick(Renderer r)
	{
		Main.player.isLooping = !Main.player.isLooping;
	}
}
