package net.trentv.vgm;

import static org.lwjgl.glfw.Callbacks.errorCallbackPrint;
import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.GLFW_RESIZABLE;
import static org.lwjgl.glfw.GLFW.GLFW_VISIBLE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.glfwSetScrollCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowSizeCallback;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.system.MemoryUtil.NULL;

import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowSizeCallback;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

public class Display implements Runnable
{
	private static GLFWErrorCallback errorCallback;
	private static GLFWKeyCallback keyCallback;
	@SuppressWarnings("unused")
	private static GLFWScrollCallback scrollCallback;
	private static GLFWMouseButtonCallback mouseButtonCallback;
	private static GLFWCursorPosCallback mousePosCallback;
	@SuppressWarnings("unused")
	private static GLFWWindowSizeCallback resizeCallback;
	private static long glfwWindowContext;
	public static boolean isInitialized = false;
	private static boolean waitingForViewport = false;

	public static int width = 800;
	public static int height = 600;
	public static String title = "MidiPlayer";

	@Override
	public void run()
	{
		Thread.currentThread().setName("Display loop");
		glfwSetErrorCallback(errorCallback = errorCallbackPrint(System.err));

		if (glfwInit() != GL11.GL_TRUE)
		{
			return;
		}

		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		glfwWindowContext = glfwCreateWindow(width, height, title, NULL, NULL);
		if (glfwWindowContext == NULL)
		{
			return;
		}

		glfwMakeContextCurrent(glfwWindowContext);
		// Vsync
		// glfwSwapInterval(0);
		glfwSetKeyCallback(glfwWindowContext, keyCallback = new GLFWKeyCallback()
		{
			@Override
			public void invoke(long window, int key, int scancode, int action, int mods)
			{
				if (action == GLFW_PRESS)
					InputMapper.setStatus(key, true);
				if (action == GLFW_RELEASE)
					InputMapper.setStatus(key, false);
			}
		});
		glfwSetMouseButtonCallback(glfwWindowContext, mouseButtonCallback = new GLFWMouseButtonCallback()
		{
			@Override
			public void invoke(long window, int button, int action, int mods)
			{
				if (action == 0)
					InputMapper.setMouseUp(button);
				if (action == 1)
					InputMapper.setMouseDown(button);
			}
		});

		glfwSetCursorPosCallback(glfwWindowContext, mousePosCallback = new GLFWCursorPosCallback()
		{
			@Override
			public void invoke(long window, double x, double y)
			{
				InputMapper.setMousePos((int) x, (int) y);
			}
		});
		glfwSetWindowSizeCallback(glfwWindowContext, resizeCallback = new GLFWWindowSizeCallback()
		{
			@Override
			public void invoke(long window, int x, int y)
			{
				width = x;
				height = y;
				waitingForViewport = true;
			}
		});
		glfwSetScrollCallback(glfwWindowContext, scrollCallback = new GLFWScrollCallback()
		{
			@Override
			public void invoke(long window, double xoffset, double yoffset)
			{
				InputMapper.setMouseWheelDelta((int) yoffset);
			}
		});
		glfwShowWindow(glfwWindowContext);

		GLContext.createFromCurrent();
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glOrtho(0, width, height, 0, 1, -1);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);

		isInitialized = true;
		while (true)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			if (waitingForViewport)
			{
				GL11.glViewport(0, 0, width, height);
			}
			Main.draw();
			glfwSwapBuffers(glfwWindowContext);
			glfwPollEvents();
			if (glfwWindowShouldClose(glfwWindowContext) == GL_TRUE)
			{
				exit();
				System.exit(0);
			}
		}
	}
	
	private static final void exit()
	{
		glfwTerminate();
		keyCallback.release();
		errorCallback.release();
		mouseButtonCallback.release();
		mousePosCallback.release();
	}
}
