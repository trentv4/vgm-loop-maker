package net.trentv.vgm;

import org.lwjgl.glfw.GLFW;

public class InputMapper
{
	private static int[] mousePos = new int[2];
	private static boolean[] mouseButtons = new boolean[10];
	private static int wheelDelta = 0;

	private static boolean[] keyHeldStatus = new boolean[348];
	private static boolean[] keyPressedStatus = new boolean[348];

	public static final void tick()
	{
		if(isPressed(GLFW.GLFW_KEY_W)) //fires only once per press of W
		{
			//do the thing
		}
		if(isDown(GLFW.GLFW_KEY_S)) //fires while W is held down
		{
			//do the thing
		}
	}
	
	/** Sets the current mouse position to {x, y}. */
	public static final void setMousePos(int x, int y)
	{
		mousePos[0] = x;
		mousePos[1] = y;
	}

	public static final void setMouseWheelDelta(int delta)
	{
		wheelDelta = delta;
	}

	/** Sets the provides <i>button</i> to "true". Used for the mouse.` */
	public static final void setMouseDown(int button)
	{
		if (mouseButtons.length >= button)
		{
			mouseButtons[button] = true;
		}
	}

	/** Sets the provides <i>button</i> to "false". */
	public static final void setMouseUp(int button)
	{
		if (mouseButtons.length >= button)
		{
			mouseButtons[button] = false;
		}
	}

	public static final boolean[] getMouseButtons()
	{
		return mouseButtons;
	}

	public static final int getWheel()
	{
		return wheelDelta;
	}

	public static final int[] getMousePos()
	{
		return mousePos;
	}

	public static final boolean isPressed(int key)
	{
		if (keyPressedStatus[key])
		{
			keyPressedStatus[key] = false;
			return true;
		}
		return false;
	}

	public static final boolean isDown(int key)
	{
		return keyHeldStatus[key];
	}

	public static final void setStatus(int key, boolean state)
	{
		try
		{
			keyHeldStatus[key] = state;
			if (state)
			{
				keyPressedStatus[key] = true;
			}
		} catch (Exception e)
		{
			// whoops!
		}
	}
}
